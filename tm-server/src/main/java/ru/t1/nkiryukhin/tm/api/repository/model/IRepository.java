package ru.t1.nkiryukhin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.model.AbstractModel;

import javax.persistence.EntityManager;

public interface IRepository<M extends AbstractModel> {

    void add(@NotNull M model);

    void remove(@NotNull M model);

    void update(@NotNull M model);

    @NotNull
    EntityManager getEntityManager();

}