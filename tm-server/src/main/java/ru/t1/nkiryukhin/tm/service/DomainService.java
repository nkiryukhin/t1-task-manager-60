package ru.t1.nkiryukhin.tm.service;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.nkiryukhin.tm.api.service.IConnectionService;
import ru.t1.nkiryukhin.tm.api.service.IDomainService;

@Service
public final class DomainService implements IDomainService {

    @NotNull
    @Autowired
    private IConnectionService connectionService;

    @Override
    @SneakyThrows
    public void createDatabase() {
        final Liquibase liquibase = connectionService.getLiquibase();
        liquibase.update("scheme");
    }

    @Override
    @SneakyThrows
    public void dropDatabase() {
        final Liquibase liquibase = connectionService.getLiquibase();
        liquibase.dropAll();
    }

}
