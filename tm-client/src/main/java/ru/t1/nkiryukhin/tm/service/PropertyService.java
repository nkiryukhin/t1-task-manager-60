package ru.t1.nkiryukhin.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;

@Getter
@Setter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @Value("#{environment['server.host']}")
    private String host;

    @Value("#{environment['server.port']}")
    private String port;

}
